package tasks.other;

import org.junit.Test;

import java.util.Collections;
import java.util.List;

import static java.util.Arrays.asList;
import static java.util.Collections.singletonList;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertThat;
import static org.hamcrest.Matchers.containsInAnyOrder;


public class R1taskTest {
    @Test
    public void shouldReturnListWithRepeatedValues() {
        // given
        R1task exercise = new R1task();

        // when
        List<Integer> result = exercise.findDuplicates(asList(-1, 1, 3, 2, 5, 6, -1, 3, 6), 2);

        // then
        assertThat(asList(-1, 3, 6), containsInAnyOrder(result.toArray()));
    }

    @Test
    public void nullValuesShouldBeOmitted() {
        // given
        R1task exercise = new R1task();

        // when
        List<Integer> result = exercise.findDuplicates(asList(1, 1, null, 2, 5, 6, 1, 3, 6, null), 2);

        // then
        assertEquals(singletonList(6), result);
    }


    @Test
    public void ShouldReturnCountOfCharacterInString(){
        R1task task = new R1task();
        //when
        String base = "aaaaacccbbbbrrraaaacc";
        char character = 'c';

        //then
        assertEquals(Long.valueOf(5),task.findRepetitionCountOfCharacterInString(base,character));
    }
}
