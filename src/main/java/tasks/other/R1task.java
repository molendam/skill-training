package tasks.other;

import java.util.*;
import java.util.function.Function;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static java.util.function.Function.identity;
import static java.util.stream.Collectors.*;

public class R1task {
    public static void main(String[] args) {

    }
    List<Integer> findDuplicates(List<Integer> values, final int repetitionCount) {
        final Map<Integer, Long> hm = values.stream()
                .filter(Objects::nonNull)
                .collect(groupingBy(
                        Function.identity(),
                        Collectors.counting()
                ));
        return hm.keySet()
                .stream()
                .filter(key -> hm.get(key).equals((long) repetitionCount))
                .collect(Collectors.toList()
                );
    }


    Long findRepetitionCountOfCharacterInString(String base, char s){
        return Stream.of(base.split(""))
                .filter(character -> character.equals(String.valueOf(s)))
                .count();
    }


}
