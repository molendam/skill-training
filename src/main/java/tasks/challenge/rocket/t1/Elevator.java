package tasks.challenge.rocket.t1;

public class Elevator {

    public static String getCloserElevator (int a_position, int b_position, int called_floor){
        int a_distance = Math.abs(called_floor-a_position);
        int b_distance = Math.abs(called_floor-b_position);

        if (b_distance>a_distance){
            return "A";
        }
        return "B";
    }
}
