package tasks.challenge.rocket.t2;

import java.util.regex.Pattern;

public class KebabCaseConverter {

    public static void main(String[] args) {
        System.out.println(convertFromCamelCase("someCamelCaseName"));
        System.out.println(convertFromCamelCase("camelCase2"));
    }

//      1 < length of the input string < 200
    public static String convertFromCamelCase(final String camelCaseName){
        StringBuilder kebabCaseName = new StringBuilder();
        Pattern bigLetterPattern = Pattern.compile("([A-Z])");
        Pattern digitPattern = Pattern.compile("([0-9])");


        for ( char x: camelCaseName.toCharArray()
             ) {
            String xStringValue = String.valueOf(x);
            if (bigLetterPattern.matcher(xStringValue).matches()){
                kebabCaseName.append("-").append(xStringValue.toLowerCase());
            }
            else if (!digitPattern.matcher(xStringValue).matches()){
                kebabCaseName.append(xStringValue);
            }
        }
        return  kebabCaseName.toString();

    }
}
